# Summon Docker
# Copyright (C) 2024 Freya Lupen <penguinflyer2222@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from krita import Extension, Krita

from PyQt5.Qt import Qt
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QTableWidget, QTableWidgetItem, QPushButton, QAbstractItemView, QCheckBox
from PyQt5.QtGui import QCursor

# toolbar
from PyQt5.QtCore import QPoint

# Disorganized work in progress.


EXTENSION_ID = 'pykrita_summon_docker'
MENU_ENTRY = 'Summon Docker'

# Constant string for the config group in kritarc
PLUGIN_CONFIG = "plugin/SummonDocker"

class DockersDialog(QDialog):

    dockerIds = []
    dockerNames = []

    lastFloated = None
    lastFloatedVisibility = False

    showAll = True
    tableWidget = None

    def __init__(self, parent):
        super().__init__(parent)
        
        self.setWindowFlag(Qt.FramelessWindowHint)

        layout = QVBoxLayout()
        self.setupDockerList()
        self.reloadList()
        layout.addWidget(self.tableWidget)

        button = QPushButton("All")
        button.setCheckable(True)
        button.toggled.connect(self.toggleAll)
        layout.addWidget(button)

        self.setLayout(layout)

        self.toggleAll(self.showAll)


    # is this too much, should it be on click outside instead?
    def leaveEvent(self, event):
        self.close()

    def unfloatDocker(self, docker):
        docker.setFloating(False)
        docker.setVisible(self.lastFloatedVisibility)
        self.lastFloated == None

    def findDocker(self, objectName):
        return next((w for w in Krita.instance().dockers() if w.objectName() == objectName), None)  

    # the availablilty of dockers is not expected to change during runtime
    # if ever reloading plugins during runtime was somehow implemented, then it would be a problem
    def setupDockerList(self):
        dockers = Krita.instance().dockers()
        for docker in dockers:
            self.dockerIds.append(docker.objectName())
            self.dockerNames.append(docker.windowTitle())
        
        self.tableWidget = QTableWidget(len(self.dockerIds),2)
        self.tableWidget.horizontalHeader().setVisible(False)
        self.tableWidget.verticalHeader().setVisible(False)
        # don't try to type in the boxes if accidentally held the shortcut key
        self.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)

        # todo dragndrop broken
        #tableWidget.setDragDropMode(QAbstractItemView.InternalMove)

        self.tableWidget.cellClicked.connect(self.activated)

    def reloadList(self):
        self.showAll = (Krita.instance().readSetting(PLUGIN_CONFIG , "showAll", "True") == True)

        showListTxt = Krita.instance().readSetting(PLUGIN_CONFIG , "dockerList", "")
        showList = showListTxt.split(',')

        # todo these are in alpha-by-id not name
        # sort them?
        i = 0
        for name in self.dockerNames:
            item = QTableWidgetItem(name)
            item.setData(Qt.UserRole, self.dockerIds[i])
            # these checkboxes are super invisible
            checkbox = QCheckBox()
            if self.dockerIds[i] in showList:
                checkbox.setChecked(True)
            self.tableWidget.setCellWidget(i, 0, checkbox)
            self.tableWidget.setItem(i, 1, item)
            i+=1
        # todo can we make the second column expand
        self.tableWidget.resizeColumnsToContents()
    

    def activated(self, row, col):
        item = self.tableWidget.item(row,col)
        if col == 1:
            print("activated", item.text(), item.data(Qt.UserRole))
            qdock = self.findDocker(item.data(Qt.UserRole))
            #oldPos = qdock.pos()
            oldVis = qdock.isVisible()
            if not qdock.isFloating() or not qdock.isVisible():
                qdock.setFloating(True)
                qdock.setVisible(True)
                qdock.move(QCursor.pos())
                self.lastFloated = item.data(Qt.UserRole)
                self.lastFloatedVisibility = oldVis
                self.close()
            else:
                self.unfloatDocker(qdock)
                self.close()


    def toggleAll(self, all):
        self.tableWidget.setColumnHidden(0, not all)
        for i in range(self.tableWidget.rowCount()):
            item = self.tableWidget.cellWidget(i,0)
            if not item.isChecked():
                self.tableWidget.setRowHidden(i, not all)
        
        # todo can we make the second column expand
        self.tableWidget.resizeColumnsToContents()

    def writeSetting(self):
        dockerList = ""
        for i in range(self.tableWidget.rowCount()):
            checkbox = self.tableWidget.cellWidget(i,0)
            if checkbox.isChecked():
                item = self.tableWidget.item(i,1)
                dockerList += item.data(Qt.UserRole)+","

        Krita.instance().writeSetting(PLUGIN_CONFIG, "dockerList", dockerList)
        Krita.instance().writeSetting(PLUGIN_CONFIG, "showAll", "True" if self.showAll else "False")




class SummonDocker(Extension):

    dialog = None

    hiddenDockers = { 1 : [],  # left
                      2 : [],  # right
                      4 : [],  # top
                      8 : [] } # bottom

    def __init__(self, parent):
        super().__init__(parent)

        self.hiddenDockers[1] = Krita.instance().readSetting(PLUGIN_CONFIG, "hiddenDockersLeft", "").split(",")
        self.hiddenDockers[2] = Krita.instance().readSetting(PLUGIN_CONFIG, "hiddenDockersRight", "").split(",")
        self.hiddenDockers[4] = Krita.instance().readSetting(PLUGIN_CONFIG, "hiddenDockersTop", "").split(",")
        self.hiddenDockers[8] = Krita.instance().readSetting(PLUGIN_CONFIG, "hiddenDockersBottom", "").split(",")

    def setup(self):
        pass

    def createActions(self, window):
        action = window.createAction(EXTENSION_ID, MENU_ENTRY, "tools/scripts")
        action.triggered.connect(self.action_triggered)

        action = window.createAction("pykrita_toggleDockersLeft", "Toggle Left Dockers", "tools/scripts")
        action.triggered.connect(self.toggleLeft)
        action = window.createAction("pykrita_toggleDockersRight", "Toggle Right Dockers", "tools/scripts")
        action.triggered.connect(self.toggleRight)
        action = window.createAction("pykrita_toggleDockersTop", "Toggle Top Dockers", "tools/scripts")
        action.triggered.connect(self.toggleTop)
        action = window.createAction("pykrita_toggleDockersBottom", "Toggle Bottom Dockers", "tools/scripts")
        action.triggered.connect(self.toggleBottom)

        # Docker toolbar stuff
        self.createActions2(window)

    def action_triggered(self):
        if not self.dialog:
            self.dialog = DockersDialog(Krita.instance().activeWindow().qwindow())
        else:
            self.dialog.reloadList()

        self.dialog.move(QCursor.pos())

        # Should this be exec, or show, or what?
        self.dialog.exec()

        self.dialog.writeSetting()

    def toggleLeft(self):
        self.toggleDockAreaVis(1)
        Krita.instance().writeSetting(PLUGIN_CONFIG, "hiddenDockersLeft", ",".join(self.hiddenDockers[1]))
    def toggleRight(self):
        self.toggleDockAreaVis(2)
        Krita.instance().writeSetting(PLUGIN_CONFIG, "hiddenDockersRight", ",".join(self.hiddenDockers[2]))
    def toggleTop(self):
        self.toggleDockAreaVis(4)
        Krita.instance().writeSetting(PLUGIN_CONFIG, "hiddenDockersTop", ",".join(self.hiddenDockers[4]))
    def toggleBottom(self):
        self.toggleDockAreaVis(8)
        Krita.instance().writeSetting(PLUGIN_CONFIG, "hiddenDockersBottom", ",".join(self.hiddenDockers[8]))

    def toggleDockAreaVis(self, area):
        dockers = Krita.instance().dockers()
        mainWindow = Krita.instance().activeWindow().qwindow()

        if len(self.hiddenDockers[area]) > 0: # show
            for dockerId in self.hiddenDockers[area]:
                docker = next((w for w in Krita.instance().dockers() if w.objectName() == dockerId), None)
                if docker:
                    docker.setVisible(True)
            self.hiddenDockers[area] = []
        else: # hide
            for docker in dockers:
                if docker.isHidden() or docker.isFloating():
                    continue
                if mainWindow.dockWidgetArea(docker) == area:
                    self.hiddenDockers[area].append(docker.objectName())
                    docker.setVisible(False)


# "Docker toolbar" stuff

    # Hardcoded list of dockers,
    # because toolbars are loaded before dockers are,
    # so they can't really be queried.

    # 5.3 prealpha list.
    # icons from the other list...
    dockersList = {
        "AnimationCurvesDocker": "",
        "ArrangeDocker": "krita_tool_grid",
        "ArtisticColorSelector": "",
        "BrushHudDocker": "",
        "ChannelDocker": "view-choose",
        "ColorSelectorNg": "",
        "CompositionDocker": "",
        "DigitalMixer": "",
        "GamutMask": "",
        "GridDocker": "",
        "HistogramDocker": "krita_tool_gradient",
        "History": "infinity",
        "KisLayerBox": "",
        "LogDocker": "",
        "LutDocker": "",
        "OnionSkinsDocker": "onion_skin_options",
        "OverviewDocker": "",
        "PaletteDocker": "",
        "PatternDocker": "pattern",
        "PresetDocker": "krita_tool_multihand",
        "PresetHistory": "",
        "RecorderDocker": "",
        "SmallColorSelector": "",
        "Snapshot": "",
        "SpecificColorSelector": "",
        "StoryboardDocker": "",
        "SvgSymbolCollectionDocker": "",
        "TasksetDocker": "",
        "TimelineDocker": "",
        "ToolBox": "",
        "WideGamutColorSelector": "",
        "comics_project_manager_docker": "",
        "lastdocumentsdocker": "",
        "quick_settings_docker": "",
        "sharedtooldocker": "properties",
    }

    dockerStates = {}

    def createActions2(self, window):
        for dockerId, icon in self.dockersList.items():
            if icon == "":
                icon = "view-list-details"
            # names are not available now...
            action = window.createAction("pykrita_toolbarDocker"+dockerId, dockerId, "")
            action.setCheckable(True)
            action.setData(dockerId)
            action.setIcon(Krita.instance().icon(icon))
            action.triggered.connect(self.bringDockerNextToToolbar)

        # we have to get the names after the dockers are loaded.
        appNotifier = Application.instance().notifier()
        appNotifier.setActive(True)
        appNotifier.windowCreated.connect(self.setActionNames)

    def setActionNames(self):
        window = Application.instance().activeWindow().qwindow()
        actions = window.actions()
        for action in actions:
            # Not really ideal.
            if action.objectName().startswith("pykrita_toolbarDocker"):
                dockerId = action.data()
                if dockerId in self.dockersList.keys():
                    docker = next((w for w in Krita.instance().dockers() if w.objectName() == dockerId), None)
                    if docker:
                        action.setText(docker.windowTitle())

    def bringDockerNextToToolbar(self, checked):
        action = self.sender()
        dockerId = action.data()

        docker = next((w for w in Krita.instance().dockers() if w.objectName() == dockerId), None)
        if not docker:
            return

        if checked == True: # Bring it near
            oldVis = docker.isVisible()
            oldFloat = docker.isFloating()
            self.dockerStates[dockerId] = {"oldVisibility": oldVis, "oldFloating": oldFloat}

            docker.setFloating(True)

            widgets = action.associatedWidgets()
            if not widgets:
                return
            for widget in widgets:
                # Find the ToolButton so we can get its position.
                if widget.inherits("QToolButton"):
                    mainWindow = Krita.instance().activeWindow().qwindow()
                    toolbarArea = mainWindow.toolBarArea(widget.parent())
                    buttonGeometry = widget.geometry()

                    # Basic positioning.
                    # It will awkwardly overlap other widgets (like scrollbars)
                    # and does not prevent going offscreen if in a corner.
                    # But it does account for the location of the toolbar
                    # to show it on the correct side and without overlapping the button.
                    if toolbarArea == 1: # Left
                        buttonPos = QPoint(buttonGeometry.right(),0)
                        buttonPos = widget.mapToGlobal(buttonPos)
                        docker.move(buttonPos.x(), buttonPos.y())
                    elif toolbarArea == 2: # Right
                        buttonPos = QPoint(buttonGeometry.left(),0)
                        buttonPos = widget.mapToGlobal(buttonPos)
                        docker.move(buttonPos.x() - docker.window().width(), buttonPos.y())
                    elif toolbarArea == 4: # Top
                        buttonPos = QPoint(0,buttonGeometry.bottom())
                        buttonPos = widget.mapToGlobal(buttonPos)
                        docker.move(buttonPos.x(), buttonPos.y())
                    elif toolbarArea == 8: # Bottom
                        buttonPos = QPoint(0,buttonGeometry.top())
                        buttonPos = widget.mapToGlobal(buttonPos)
                        docker.move(buttonPos.x(), buttonPos.y() - docker.window().height())
                    # else:
                    # if the toolbar is floating, it will report the last area it was docked.

            docker.setVisible(True)

        else: # Send it back
            # (of course we don't know if
            #  the user already did something with it in the meantime,
            #  so idea of a toggle off is a bit flawed)
            oldVis = self.dockerStates[dockerId]["oldVisibility"]
            if oldVis == False:
                docker.setVisible(False)
            docker.setFloating(self.dockerStates[dockerId]["oldFloating"])
            if oldVis == True:
                docker.setVisible(True)
            self.dockerStates.pop(dockerId)
