from .summon_docker import SummonDocker

# And add the extension to Krita's list of extensions:
app = Krita.instance()
# Instantiate your class:
extension = SummonDocker(parent = app)
app.addExtension(extension)
